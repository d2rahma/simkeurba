<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class hakakses extends CI_Controller {

	var $template = 'template';
	
	private $_path_page = "pages/hakakses/";
	private $_path_js = "administrator/";
	private $_judul = 'Data Hak Akses';
	private $_page_index = 'hakakses';
	private $_page_form = 'form_hakakses';
	private $_table = "hakakses";


	function __construct()
	{
		parent::__construct();
		$this->load->model('model_menu','',TRUE);
		$this->load->model('model_master','',TRUE);
		$this->load->model('model_hakakses','',TRUE);
	}

	private function get_master($pages) 
	{
		$session_data = $this->session->userdata('logged_in');

		$menu = $this->model_menu->get_menu_by_susrSgroupNama($session_data['susrSgroupNama']); //pengambilan menu dari database			

		$uriS = $this->uri->segment_array();
		$data['uri']=$uriS;
		$currMod = $uriS[1];
		$otentifikasi_menu = $this->model_master->otentifikasi_menu_by_susrSgroupNama($session_data['susrSgroupNama'],$currMod); //cek otentifikasi hak akses user modul	
		$hakakses = $this->model_master->get_hakakses();

		if(!$otentifikasi_menu)
			$data['page'] = 'error_page'; //error 404
		else 
		{
			$data['page'] = $pages;
			$data['breadcrumb'] = $otentifikasi_menu[0];
		}				

		$data['susrNama'] = $session_data['susrNama'];
		$data['susrSgroupNama'] = $session_data['susrSgroupNama'];
		$data['susrProfil'] = $session_data['susrProfil'];
		$data['menu'] = $menu;
		$data['hakakses'] = $hakakses;
		$data['judul'] = $this->_judul;
		
		return $data;
	}

	public function index()
	{		
		if($this->session->userdata('logged_in')) //cek user logged
		{
			$data = $this->get_master($this->_path_page.$this->_page_index);
			$data['scripts'] = array($this->_path_js.$this->_page_index);
			$data['response_url'] = site_url($this->_page_index.'/index_response');
			$data['add_url'] = site_url('hakakses/tambah_hakakses').'/';
			$data['edit_url'] = site_url('hakakses/edit_hakakses').'/';
			$data['delete_url'] = site_url('hakakses/hapus_hakakses').'/';
			$this->load->view($this->template, $data);
		}
		else
		{
			redirect('login','refresh');
		}
	}
	
	
	public function edit_hakakses()
	{		
		if($this->session->userdata('logged_in')) //cek user logged
		{	
				$data = $this->get_master('pages/hakakses/hakakses_editHakakses');
			    $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
			    $key = explode(';',$keyS);
			    $dataHak = $this->model_hakakses->get_hakakses_by_sgroupnama($key[0]);
			    $dataHakAkses = $dataHak[0];
				$data['sgroupNama']=$dataHakAkses->sgroupNama;
				$data['sgroupKeterangan']=$dataHakAkses->sgroupKeterangan;
				$data['scripts'] = array($this->_path_js.$this->_page_index);
				$this->load->view($this->template, $data);
		}
		else
			redirect('login','refresh');
	}
	
	public function proses_edit_hakakses()
	{		
		if($this->session->userdata('logged_in')) //cek user logged
		{	
				
							
			    $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
			    $key = explode(';',$keyS);
				$sgroupNama = $this->input->post('sgroupNama');
				$sgroupKeterangan = $this->input->post('sgroupKeterangan');		
			    $updateHak = $this->model_hakakses->edit_hakakses($key[0],$sgroupNama,$sgroupKeterangan);
				
				$data = $this->get_master($this->_path_page.$this->_page_index);
				if ($updateHak) 
				echo message('Hak Akses Berhasil Diubah','success');
				else
				echo message('Hak Akses Gagal Diubah','danger');			
		}
		else
			redirect('login','refresh');
	}
	
	
	public function tambah_hakakses()
	{		
		if($this->session->userdata('logged_in')) //cek user logged
		{
			$data = $this->get_master('pages/hakakses/hakakses_tambahHakakses');	
			$data['scripts'] = array($this->_path_js.$this->_page_index);	
			$data['save_url'] = site_url($this->_page_index.'/proses_tambah_hakakses').'/';	
			$this->load->view($this->template, $data);
		}
		else
		{
			redirect('login','refresh');
		}
	}
	
	
	public function proses_tambah_hakakses()
	{		
		if($this->session->userdata('logged_in')) //cek user logged
		{					
				$sgroupNama = $this->input->post('sgroupNama');
				$sgroupKeterangan = $this->input->post('sgroupKeterangan');
				$data['scripts'] = array($this->_path_js.$this->_page_index);		
			    $tambahHak = $this->model_hakakses->tambah_hakakses($sgroupNama,$sgroupKeterangan);
				
				if ($tambahHak) 
					echo message('Hak Akses Berhasil Disimpan','success');
				else
					echo message('Hak Akses Gagal Disimpan','danger');

				//redirect(base_url().'fakultas/','refresh');
		}
		else
			redirect('login','refresh');
	}
	
	
	public function hapus_hakakses()
	{
		if($this->session->userdata('logged_in'))
		{
			$id = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
			if ($this->model_hakakses->hapus_hakakses($id)) 
				echo message('Hak Akses Berhasil Dihapus','success');
			else
				echo message('Hak Akses Gagal Dihapus','danger');	
			
			$hakakses = $this->model_master->get_hakakses();
			$data['hakakses'] = $hakakses;
			$data['add_url'] = site_url('hakakses/tambah_hakakses').'/';
			$data['edit_url'] = site_url('hakakses/edit_hakakses').'/';
			$data['delete_url'] = site_url('hakakses/hapus_hakakses').'/';
			$this->load->view('pages/hakakses/hakakses', $data);		
		}
		else
			redirect('login','refresh');
		
	}
	
}
