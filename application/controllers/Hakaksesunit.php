<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class hakaksesunit extends CI_Controller {

	var $template = 'template';
	
	private $_path_page = "pages/hakaksesunit/";
	private $_path_js = "administrator/";
	private $_judul = 'Data Hak Akses Unit';
	private $_page_index = 'hakaksesunit';
	private $_page_form = 'form_hakakses';
	private $_table = "hakakses";


	function __construct()
	{
		parent::__construct();
		$this->load->model('model_menu','',TRUE);
		$this->load->model('model_master','',TRUE);
		$this->load->model('model_hakaksesunit','',TRUE);
	}

	private function get_master($pages) 
	{
		$session_data = $this->session->userdata('logged_in');

		$menu = $this->model_menu->get_menu_by_susrSgroupNama($session_data['susrSgroupNama']); //pengambilan menu dari database			

		$uriS = $this->uri->segment_array();
		$data['uri']=$uriS;
		$currMod = $uriS[1];
		$otentifikasi_menu = $this->model_master->otentifikasi_menu_by_susrNama($session_data['susrNama'],$currMod); //cek otentifikasi hak akses user modul	
		$hakakses = $this->model_master->get_hakakses();

		if(!$otentifikasi_menu)
			$data['page'] = 'error_page'; //error 404
		else 
		{
			$data['page'] = $pages;
			$data['breadcrumb'] = $otentifikasi_menu[0];
		}				

		$data['susrNama'] = $session_data['susrNama'];
		$data['susrSgroupNama'] = $session_data['susrSgroupNama'];
		$data['susrProfil'] = $session_data['susrProfil'];
		$data['menu'] = $menu;
		$data['hakakses'] = $hakakses;
		$data['judul'] = $this->_judul;
		
		return $data;
	}

	public function index()
	{		
		if($this->session->userdata('logged_in')) //cek user logged
		{
			$data = $this->get_master($this->_path_page.$this->_page_index);
			$data['scripts'] = array($this->_path_js.$this->_page_index);
			$data['response_url'] = site_url($this->_page_index.'/index_response');
			$this->load->view($this->template, $data);
		}
		else
		{
			redirect('login','refresh');
		}
	}
	
	public function tampil_aksesunit()
	{		
	
		if($this->session->userdata('logged_in')) //cek user logged
		{					
			$this->form_validation->set_error_delimiters('<div class="uk-alert uk-alert-danger" data-uk-alert><button class="uk-alert-close uk-close"></button><span>', '</span></div>');

			$this->form_validation->set_rules('sgroupNama','sgroupNama','trim|required|xss_clean');

			if($this->form_validation->run()) 
			{
				
				if(IS_AJAX)				
	            {
					
					$sgroupNama = $this->input->post('sgroupNama');				
					$get_prodi = $this->model_hakaksesunit->get_prodi($sgroupNama);					
					$data['get_prodi'] = $get_prodi;
					$data['simpan_url'] = site_url('hakaksesunit/simpan_aksesunit').'/';
					$data['sgroupNama']=$sgroupNama;
					$pages = 'pages/hakaksesunit/hakaksesunit_response';
	                $this->load->view($pages,$data);

	            }
	            else
	            {
	                $this->index();
	            }				
			}
			else 
			{
	           $this->form_validation->set_message('check_database','Ooops!! Something Wrong!!');
			}
		}
		else
		{
			redirect('login','refresh');
		}
	}
			
	public function simpan_aksesunit()
	{
		if($this->session->userdata('logged_in'))
		{
			$id = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
			$cekProdi = $this->input->post('cekProdi');
			$sgroupnama = $this->input->post('sgroupnama');

			$this->model_hakaksesunit->hapus_aksesprodi($sgroupnama);
			
			foreach ($cekProdi as $cek => $prodi) {			 
			  $this->model_hakaksesunit->simpan_aksesprodi($sgroupnama,$prodi);
		  	}
			echo message('Data Akses Prodi Berhasil Diperbaharui','success');
		}
		else
			redirect('login','refresh');
		
	}
	
}
