<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	var $template = 'template';

	private $_path_page = "pages/home/";
	private $_path_js = "home/";
	private $_judul = 'Dashboard';
	private $_page_index = 'home';
	private $_page_opr = 'home_opr';

	function __construct()
	{
		parent::__construct();
		$this->load->model('model_menu','',TRUE);
		$this->load->model('model_master','',TRUE);
		$this->load->model('model_home','',TRUE);
	}

	private function get_master($pages) 
	{
		$session_data = $this->session->userdata('logged_in');

		$menu = $this->model_menu->get_menu_by_susrSgroupNama($session_data['susrSgroupNama']); //pengambilan menu dari database											

		$data['page'] = $pages;
		$data['susrNama'] = $session_data['susrNama'];
		$data['susrSgroupNama'] = $session_data['susrSgroupNama'];
		$data['susrProfil'] = $session_data['susrProfil'];

		$data['menu'] = $menu;
		$data['judul'] = $this->_judul;

		return $data;
	}

	public function index()
	{
		if($this->session->userdata('logged_in')) //cek user logged
		{
			$data = $this->get_master($this->_path_page.$this->_page_index);
			$data['scripts'] = array($this->_path_js.'profile');
			$this->load->view($this->template, $data);
		}
		else
		{
			redirect('login','refresh');
		}
	}

	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('home','refresh');
	}
	function ubahpass()
	{
		
		if($this->session->userdata('logged_in')) //cek user logged
		{
			$session_data = $this->session->userdata('logged_in');
			$data['susrNama'] = $session_data['susrNama'];
			$data['susrProfil'] = $session_data['susrProfil'];
			$data = $this->get_master('pages/ubahpassword/ubahpassword');	
			$data['scripts'] = array($this->_path_js."ubahpassword");		
			$this->load->view($this->template, $data);
		}
		else
		{
			redirect('login','refresh');
		}
	}
	function proses_ubah_password()
	{
		
		if($this->session->userdata('logged_in')) //cek user logged
		{
			$keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
			$susrPasswordLama = md5($this->input->post('susrPasswordLama'));
			$susrPassword = md5($this->input->post('susrPassword'));			
			
			$cek_user = $this->model_home->cek_user($keyS,$susrPasswordLama);
			if($cek_user>0)
			{
				$ubahPass = $this->model_home->ubah_password($keyS,$susrPasswordLama,$susrPassword);
				if ($ubahPass) 
				echo message('Password Berhasil Diubah','success');
				else
				echo message('Password Gagal diubah','danger');
			}
			else
			{
				echo message('Username dan Password Lama Salah','danger');
			}
		}
		else
		{
			redirect('login','refresh');
		}
	}
}
