<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class frontend extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	var $template = 'frontend';

	private $_path_page = "pages/frontend/";
	private $_path_js = "frontend/";
	private $_judul = 'Pengumuman';
	private $_page_index = 'index';

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('model_menu','',TRUE);
		$this->load->model('model_master','',TRUE);
		$this->load->model('model_frontend','',TRUE);
	}

	private function get_master($pages) 
	{
		$menu = $this->model_menu->get_menu_non_login(); //pengambilan menu dari database		
		$data['page'] = $pages;

		$data['menu'] = $menu;
		$data['judul'] = $this->_judul;

		return $data;
	}

	public function index()
	{
		if($this->session->userdata('logged_in')) //cek user logged
		{
			$data = $this->get_master($this->_path_page.$this->_page_index);
			$data['scripts'] = array($this->_path_js.'profile');
			$this->load->view($this->template, $data);
		}
		else
		{
			redirect('login','refresh');
		}		
	}
	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('home','refresh');
	}
}
