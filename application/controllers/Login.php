<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if($this->session->userdata('logged_in'))
		{
			redirect('home','refresh');
		}
		else
		{			
			$this->load->view('login');
		}
	}
}
?>