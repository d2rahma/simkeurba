<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class mapunit extends CI_Controller {

	var $template = 'template';
	
	private $_path_page = "pages/mapunit/";
	private $_path_js = "administrator/";
	private $_judul = 'Data Mapping Unit';
	private $_page_index = 'mapunit';
	private $_page_form = 'form_mapunit';


	function __construct()
	{
		parent::__construct();
		$this->load->model('model_menu','',TRUE);
		$this->load->model('model_master','',TRUE);
		$this->load->model('model_mapunit','',TRUE);
	}

	private function get_master($pages) 
	{
		$session_data = $this->session->userdata('logged_in');

		$menu = $this->model_menu->get_menu_by_susrSgroupNama($session_data['susrSgroupNama']); //pengambilan menu dari database			

		$uriS = $this->uri->segment_array();
		$data['uri']=$uriS;
		$currMod = $uriS[1];
		$otentifikasi_menu = $this->model_master->otentifikasi_menu_by_susrSgroupNama($session_data['susrSgroupNama'],$currMod); //cek otentifikasi hak akses user modul	
		$mapunit = $this->model_mapunit->get_prodi();
		
		if(!$otentifikasi_menu)
			$data['page'] = 'error_page'; //error 404
		else 
		{
			$data['page'] = $pages;
			$data['breadcrumb'] = $otentifikasi_menu[0];
		}				

		$data['susrNama'] = $session_data['susrNama'];
		$data['susrSgroupNama'] = $session_data['susrSgroupNama'];
		$data['susrProfil'] = $session_data['susrProfil'];
		$data['menu'] = $menu;
		$data['mapunit'] = $mapunit;		
		$data['judul'] = $this->_judul;
		
		return $data;
	}

	public function index()
	{		
		if($this->session->userdata('logged_in')) //cek user logged
		{
			$data = $this->get_master($this->_path_page.$this->_page_index);
			$data['scripts'] = array($this->_path_js.$this->_page_index);
			$data['response_url'] = site_url($this->_page_index.'/index_response');
			$data['add_url'] = site_url('mapunit/tambah_mapunit').'/';
			$data['edit_url'] = site_url('mapunit/edit_mapunit').'/';
			$data['delete_url'] = site_url('mapunit/hapus_mapunit').'/';
			$this->load->view($this->template, $data);
		}
		else
		{
			redirect('login','refresh');
		}
	}
	
	
	public function edit_mapunit()
	{		
		if($this->session->userdata('logged_in')) //cek user logged
		{	
				$data = $this->get_master('pages/mapunit/mapunit_editMapunit');
			    $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
			    $key = explode(';',$keyS);
			    $dtProdi = $this->model_mapunit->get_prodi_by_prodikode($key[0]);
			    $dataProdi= $dtProdi[0];
				$data['prodiKode']=$dataProdi->prodiKode;
				$data['prodiNamaResmi']=$dataProdi->prodiNamaResmi;
				$data['prodiJjarKode']=$dataProdi->prodiJjarKode;				
				
				$mapunit = $this->model_mapunit->get_prodi();
				$data['mapunit'] = $mapunit;
				$data['scripts'] = array($this->_path_js.$this->_page_index);
				$this->load->view($this->template, $data);
		}
		else
			redirect('login','refresh');
	}
	
	public function proses_edit_mapunit()
	{		
		if($this->session->userdata('logged_in')) //cek user logged
		{	
				
							
			    $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
			    $key = explode(';',$keyS);
				$prodiParent = $this->input->post('prodiParent');		
			    $updateUnit = $this->model_mapunit->edit_mapunit($key[0],$prodiParent);
				
				$data = $this->get_master($this->_path_page.$this->_page_index);
				if ($updateUnit) 
				echo message('Mapping Unit Berhasil Disimpan','success');
				else
				echo message('Mapping Unit Gagal Disimpan','danger');			
		}
		else
			redirect('login','refresh');
	}			
}
