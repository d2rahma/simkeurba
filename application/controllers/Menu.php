<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class menu extends CI_Controller {

	var $template = 'template';
	
	private $_path_page = "pages/menu/";
	private $_path_js = "frontend/";
	private $_judul = 'Menu Frontend';
	private $_page_index = 'index';
	private $_page_form = 'form';
	private $_table = "s_user_modul_ref";


	function __construct()
	{
		parent::__construct();
		$this->load->model('model_menu','',TRUE);
		$this->load->model('model_master','',TRUE);
		$this->load->model('model_menu_frontend','',TRUE);
	}

	private function get_master($pages) 
	{
		$session_data = $this->session->userdata('logged_in');

		$menu = $this->model_menu->get_menu_by_susrSgroupNama($session_data['susrSgroupNama']); //pengambilan menu dari database			

		$uriS = $this->uri->segment_array();
		$data['uri']=$uriS;
		$currMod = $uriS[1];
		$otentifikasi_menu = $this->model_master->otentifikasi_menu_by_susrSgroupNama($session_data['susrSgroupNama'],$currMod); //cek otentifikasi hak akses user modul	
		$menu_frontend = $this->model_menu_frontend->get_menu();

		if(!$otentifikasi_menu)
			$data['page'] = 'error_page'; //error 404
		else 
		{
			$data['page'] = $pages;
			$data['breadcrumb'] = $otentifikasi_menu[0];
		}				

		$data['susrNama'] = $session_data['susrNama'];
		$data['susrSgroupNama'] = $session_data['susrSgroupNama'];
		$data['susrProfil'] = $session_data['susrProfil'];
		$data['menu'] = $menu;
		$data['menu_frontend'] = $menu_frontend;
		$data['judul'] = $this->_judul;
		
		return $data;
	}

	public function index()
	{		
		if($this->session->userdata('logged_in')) //cek user logged
		{
			$data = $this->get_master($this->_path_page.$this->_page_index);
			$data['scripts'] = array($this->_path_js.'menu');
			$data['response_url'] = site_url($this->_page_index.'/reponse');
			$data['add_url'] = site_url('menu/tambah').'/';
			$data['edit_url'] = site_url('menu/ubah').'/';
			$data['delete_url'] = site_url('menu/hapus').'/';
			$data['content_url'] = site_url('menu/content').'/';
			$this->load->view($this->template, $data);
		}
		else
		{
			redirect('login','refresh');
		}
	}

	public function tambah()
	{		
		if($this->session->userdata('logged_in')) //cek user logged
		{
			$data = $this->get_master($this->_path_page.'form');	
			$data['scripts'] = array($this->_path_js.'menu');	
			$data['save_url'] = site_url('menu/save').'/';	
			$data['status_page'] = 'Tambah';
			$this->load->view($this->template, $data);
		}
		else
		{
			redirect('login','refresh');
		}
	}
	
	
	public function ubah()
	{		
		if($this->session->userdata('logged_in')) //cek user logged
		{
			$id = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));

			$data = $this->get_master($this->_path_page.'form');	

			$data['row'] = $this->model_menu_frontend->get_menu_by_susrmodulNama($id);
			$data['scripts'] = array($this->_path_js.'menu');	
			$data['save_url'] = site_url('menu/save').'/';	
			$data['status_page'] = 'Ubah';
			$this->load->view($this->template, $data);
		}
		else
		{
			redirect('login','refresh');
		}
	}
	
	
	public function save()
	{		
		if($this->session->userdata('logged_in')) //cek user logged
		{	
			$this->form_validation->set_rules('susrmodulNamaDisplay','susrmodulNamaDisplay','trim|required|xss_clean');
			$this->form_validation->set_rules('susrmodulUrut','susrmodulUrut','trim|required|xss_clean');

			if($this->form_validation->run() == FALSE) {
				$susrmodulNamaDisplay = $this->input->post('susrmodulNamaDisplay');
				$susrmodulUrut = $this->input->post('susrmodulUrut');
				$susrmodulNama = $this->input->post('susrmodulNama');
				$susrmodulNamaNew = strtolower(str_replace(' ', '', $susrmodulNamaDisplay));

				if(empty($susrmodulNama)) {
					$param = array(
							'susrmodulNamaDisplay'=>$susrmodulNamaDisplay,
							'susrmodulUrut'=>$susrmodulUrut,
							'susrmodulNama'=>$susrmodulNamaNew,
							'susrmodulSusrmdgroupNama'=>'frontend',
							'susrmodulIsLogin'=>0

						);
					$proses = $this->model_menu_frontend->tambah($param);
				} else {
					$param = array(
							'susrmodulNamaDisplay'=>$susrmodulNamaDisplay,
							'susrmodulUrut'=>$susrmodulUrut,
							'susrmodulNama'=>$susrmodulNamaNew
						);
					$proses = $this->model_menu_frontend->ubah($susrmodulNama,$param);
				}
				if ($proses) 
					echo message($this->_judul.' Berhasil Disimpan','success');
				else
					echo message($this->_judul.' Gagal Disimpan','danger');
			} else
				redirect('home','refresh');	
		}
		else
			redirect('login','refresh');
	}
	
	
	public function hapus($id = '')
	{
		if($this->session->userdata('logged_in'))
		{
			$id = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
			if ($this->model_hakakses->hapus_hakakses($id)) 
				$data['pesan']=message('Menu Frontend Berhasil Dihapus','success');
			else
				$data['pesan']=message('Menu Frontend Gagal Dihapus','danger');	
			
			$menu_frontend = $this->model_menu_frontend->get_menu();
			$data['menu_frontend'] = $menu_frontend;
			$data['add_url'] = site_url('menu/tambah').'/';
			$data['edit_url'] = site_url('menu/ubah').'/';
			$data['delete_url'] = site_url('menu/hapus').'/';
			$data['content_url'] = site_url('menu/content').'/';
			$this->load->view($this->_path_page.'/response', $data);		
		}
		else
			redirect('login','refresh');
		
	}

	public function content()
	{		
		if($this->session->userdata('logged_in')) //cek user logged
		{
			$id = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
			$row = $this->model_menu_frontend->get_content($id);
			$menu = $this->model_menu_frontend->get_menu_by_susrmodulNama($id);

			$data = $this->get_master($this->_path_page.'/content');
			$data['cSusrmodulNama'] = $id;
			$data['cJudul'] = $menu!==false ? $menu->susrmodulNamaDisplay : '';
			$data['datas'] = $row;
			$data['scripts'] = array($this->_path_js.'menu');
			$data['save_url'] = site_url('menu/savecontent').'/';
			$data['status_page'] = 'Content';
			$this->load->view($this->template, $data);
		}
		else
		{
			redirect('login','refresh');
		}
	}

	public function savecontent()
	{		
		if($this->session->userdata('logged_in')) //cek user logged
		{	
			$session_data = $this->session->userdata('logged_in');


			$this->form_validation->set_rules('cJudul','cJudul','');
			$this->form_validation->set_rules('cIsi','cIsi','');
			$this->form_validation->set_rules('cSusrmodulNama','cSusrmodulNama','');

			if($this->form_validation->run() == FALSE) {
				$cJudul = $this->input->post('cJudul');
				$cIsi = $this->input->post('cIsi');
				$cTag = $this->input->post('cTag');
				$cSusrmodulNama = $this->input->post('cSusrmodulNama');

				$cek = $this->model_menu_frontend->get_content($cSusrmodulNama);

				if($cek===false) {
					$param = array(
							'cJudul'=>$cJudul,
							'cIsi'=>$cIsi,
							'cTag'=>$cTag,
							'cSusrmodulNama'=>$cSusrmodulNama,
							'cAuthor'=>$session_data['susrNama'],
							'cDatetime'=>date('Y-m-d H:i:s')

						);
					$proses = $this->model_menu_frontend->tambahcontent($param);
				} else {
					$param = array(
							'cJudul'=>$cJudul,
							'cIsi'=>$cIsi,
							'cTag'=>$cTag

						);
					$proses = $this->model_menu_frontend->ubahcontent($cSusrmodulNama,$param);
				}
				if ($proses) 
					echo message('Content Berhasil Disimpan','success');
				else
					echo message('Content Gagal Disimpan','danger');
			} else
				echo message('Content Gagal Disimpan','danger');
		}
		else
			redirect('login','refresh');
	}
	
}
