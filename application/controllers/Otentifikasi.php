<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class otentifikasi extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('model_login','',TRUE);
	}

	public function index()
	{

		$this->form_validation->set_error_delimiters('<div class="uk-alert uk-alert-danger" data-uk-alert><button class="uk-alert-close uk-close"></button><span>', '</span></div>');
		
		$this->form_validation->set_rules('username','username','trim|required|xss_clean');
		$this->form_validation->set_rules('password','password','trim|required|xss_clean|callback_check_database');

		if($this->form_validation->run() == FALSE)
			$this->load->view('login');
		else
			redirect('home','refresh');
	}

	function check_database($password)
	{
		$username = $this->input->post('username');
		$result = $this->model_login->get_user_login($username,$password);

		$this->load->library('Serviceclient');
		$pegawai = $this->serviceclient->getPegawaiByNip($username);

		if($result)
		{
			$sess_array = array();
			foreach($result as $row)
			{								
				$sess_array = array(
					'susrNama' => $row->susrNama,
					'susrSgroupNama' => $row->susrSgroupNama,
					'susrProfil' => $row->susrProfil
				);
				$this->session->set_userdata('logged_in',$sess_array);
				return TRUE;
			}
		}
		elseif($pegawai!=false and is_array($pegawai))
		{
			if($pegawai['PASSWORD']==md5($password))
			{
				if($pegawai['KODE_TENAGA']==2 and !empty($pegawai['KODE_JABTAM']))
					$susrSgroupNama = 'DOSEN_STRUKTURAL';
				elseif($pegawai['KODE_TENAGA']==2 and empty($pegawai['KODE_JABTAM']))
					$susrSgroupNama = 'DOSEN';
				elseif($pegawai['KODE_TENAGA']==1)
					$susrSgroupNama = 'STRUKTURAL';
				else
					$susrSgroupNama = 'UNKNOWN';
				$sess_array = array(
					'susrNama' => $pegawai['NIP'],
					'susrSgroupNama' => $susrSgroupNama,
					'susrProfil' => $pegawai['NAMA_PEGAWAI'],
					'kode_unit' => $pegawai['KODE_UNIT']
				);
				$this->session->set_userdata('logged_in',$sess_array);
			} 
			else
			{
				$this->form_validation->set_message('check_database','Invalid Username or Password');
				return FALSE;
			}
		}
		else
		{	
			$this->form_validation->set_message('check_database','Invalid Username or Password');
			return FALSE;
		}
	}	
}
?>