<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class pengguna extends CI_Controller {

	var $template = 'template';
	
	private $_path_page = "pages/pengguna/";
	private $_path_js = "administrator/";
	private $_judul = 'Data Pengguna';
	private $_page_index = 'pengguna';
	private $_page_form = 'form_pengguna';
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('model_menu','',TRUE);
		$this->load->model('model_master','',TRUE);
		$this->load->model('model_pengguna','',TRUE);
	}

	private function get_master($pages) 
	{
		$session_data = $this->session->userdata('logged_in');

		$menu = $this->model_menu->get_menu_by_susrSgroupNama($session_data['susrSgroupNama']); //pengambilan menu dari database			

		$uriS = $this->uri->segment_array();
		$data['uri']=$uriS;
		$currMod = $uriS[1];
		$otentifikasi_menu = $this->model_master->otentifikasi_menu_by_susrSgroupNama($session_data['susrSgroupNama'],$currMod); //cek otentifikasi hak akses user modul	
		
		if($session_data['susrSgroupNama']=='ADMIN')
		$hakakses = $this->model_master->get_hakakses();
		else
		$hakakses = $this->model_pengguna->get_hakakses_by_id($session_data['susrSgroupNama']);

		if(!$otentifikasi_menu)
			$data['page'] = 'error_page'; //error 404
		else 
		{
			$data['page'] = $pages;
			$data['breadcrumb'] = $otentifikasi_menu[0];
		}				

		$data['susrNama'] = $session_data['susrNama'];
		$data['susrSgroupNama'] = $session_data['susrSgroupNama'];
		$data['susrProfil'] = $session_data['susrProfil'];
		$data['menu'] = $menu;
		$data['hakakses'] = $hakakses;
		$data['judul'] = $this->_judul;

		return $data;
	}

	public function index()
	{		
		if($this->session->userdata('logged_in')) //cek user logged
		{
			$data = $this->get_master('pages/pengguna/pengguna');
			$data['scripts'] = array($this->_path_js.$this->_page_index);
			$this->load->helper("generate");
			$this->load->view($this->template, $data);

		}
		else
		{
			redirect('login','refresh');
		}
	}
	public function tampilpengguna()
	{		
		if($this->session->userdata('logged_in')) //cek user logged
		{			
			$this->form_validation->set_error_delimiters('<div class="uk-alert uk-alert-danger" data-uk-alert><button class="uk-alert-close uk-close"></button><span>', '</span></div>');

			$this->form_validation->set_rules('sgroupNama','sgroupNama','trim|required|xss_clean');

			if($this->form_validation->run()) 
			{
				if(IS_AJAX)
	            {

					$sgroupNama = $this->input->post('sgroupNama');				

					$pengguna = $this->model_pengguna->get_pengguna($sgroupNama);
					$data['pengguna'] = $pengguna;	
					$data['sgroupNama'] = $sgroupNama;					
					$data['edit_url'] = site_url($this->_page_index.'/edit_pengguna').'/';
					$data['del_url'] = site_url($this->_page_index.'/hapus_pengguna').'/';
					$data['add_url'] = site_url($this->_page_index.'/tambah_pengguna').'/';
					$data['reset'] = site_url($this->_page_index.'/reset_pengguna').'/';
					$pages = 'pages/pengguna/pengguna_response';
	                $this->load->view($pages,$data);
	            }
	            else
	            {
	                $this->index();
	            }				
			}
			else 
			{
	           $this->form_validation->set_message('check_database','Ooops!! Something Wrong!!');
			}
		}
		else
		{
			redirect('login','refresh');
		}
	}
	
	public function tambah_pengguna()
	{		
		if($this->session->userdata('logged_in')) //cek user logged
		{
			$this->load->helper("generate");
			$data = $this->get_master('pages/pengguna/pengguna_tambah');
			$id = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
			$data['sgroupNama']=$id;
			$data['password']=generatePassword();
			$data['scripts'] = array($this->_path_js.$this->_page_index);
			$this->load->view($this->template, $data);
		}
		else
		{
			redirect('login','refresh');
		}
	}
	
	public function proses_tambah_pengguna($sgroupNama)
	{		
		if($this->session->userdata('logged_in')) //cek user logged
		{	
				$susrNama = $this->input->post('susrNama');
				$susrProfil = $this->input->post('susrProfil');
				$susrPassword = $this->input->post('susrPassword');				
				$data['scripts'] = array($this->_path_js.$this->_page_index);	

			    $tambahPengguna = $this->model_pengguna->tambah_pengguna($susrNama,md5($susrPassword),$sgroupNama,$susrProfil);
				
				if ($tambahPengguna) 
					echo message('Pengguna Berhasil Ditambah','success');
				else
					echo message('Pengguna Gagal Ditambah','danger');
		}
		else
			redirect('login','refresh');
	}
	
	public function edit_pengguna()
	{		
		if($this->session->userdata('logged_in')) //cek user logged
		{	
				$data = $this->get_master('pages/pengguna/pengguna_edit');
			    $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
			    $key = explode(';',$keyS);
			    $dataPengguna = $this->model_pengguna->get_pengguna_by_id($key[0]);
			    $pengguna = $dataPengguna[0];
				$data['susrNama']=$pengguna->susrNama;
				$data['susrProfil']=$pengguna->susrProfil;
				$data['susrSgroupNama']=$pengguna->susrSgroupNama;
				$hakakses = $this->model_master->get_hakakses();
				$data['hakakses'] = $hakakses;
				$data['scripts'] = array($this->_path_js.$this->_page_index);
				$this->load->view($this->template, $data);
		}
		else
			redirect('login','refresh');
	}
	
	public function proses_edit_pengguna()
	{		
		if($this->session->userdata('logged_in')) //cek user logged
		{		
				$keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
			    $key = explode(';',$keyS);
				$susrNama = $this->input->post('susrNama');
				$susrProfil = $this->input->post('susrProfil');
				$susrSgroupNama = $this->input->post('susrSgroupNama');				
			    $updatePengguna = $this->model_pengguna->edit_pengguna($key[0],$susrProfil,$susrSgroupNama);
				$data = $this->get_master($this->_path_page.$this->_page_index);				
				if ($updatePengguna) 
				echo message('Pengguna Berhasil Diubah','success');
				else
				echo message('Pengguna Gagal Diubah','danger');
		}
		else
			redirect('login','refresh');
	}
	
	
	public function hapus_pengguna()
	{		
		if($this->session->userdata('logged_in')) //cek user logged
		{			
			$session_data = $this->session->userdata('logged_in');
			if(IS_AJAX)
            {
            	$keys = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
				$key=explode(";",$keys);
				$sgroupNama=$key[1];
				$param = array (
						'susrNama'=>$key[0]
					);
				$hapusPengguna = $this->model_pengguna->hapus_pengguna($param);
			    if($hapusPengguna)
        			echo message('Pengguna Berhasil Dihapus','success');
        		else
        			echo message('Pengguna Gagal Dihapus','danger');
				
				
				$pengguna = $this->model_pengguna->get_pengguna($sgroupNama);
					$data['pengguna'] = $pengguna;	
					$data['sgroupNama'] = $sgroupNama;					
					$data['edit_url'] = site_url($this->_page_index.'/edit_pengguna').'/';
					$data['del_url'] = site_url($this->_page_index.'/hapus_pengguna').'/';
					$data['add_url'] = site_url($this->_page_index.'/tambah_pengguna').'/';
					$data['reset'] = site_url($this->_page_index.'/reset_pengguna').'/';
				$pages = 'pages/pengguna/pengguna_response';
                $this->load->view($pages,$data);
            }
            else
            {
                $this->index();
            }	
		}
		else
		{
			redirect('login','refresh');
		}
	}
	
	
	
	public function reset_pengguna()
	{		
		if($this->session->userdata('logged_in')) //cek user logged
		{			
			$session_data = $this->session->userdata('logged_in');
			if(IS_AJAX)
            {
				$this->load->helper("generate");
            	$keys = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
				$key=explode(";",$keys);
				$dataPengguna = $this->model_pengguna->get_pengguna_by_id($key[0]);
			    $pengguna = $dataPengguna[0];
				$data['susrNama']=$pengguna->susrNama;
				$data['susrProfil']=$pengguna->susrProfil;
				$data['susrSgroupNama']=$pengguna->susrSgroupNama;
				$data['susrPassword']=generatePassword();
				$this->model_pengguna->update_password($data['susrNama'],$data['susrPassword']);
						
				$data['scripts'] = array($this->_path_js.$this->_page_index);
				$this->load->view('pages/pengguna/pengguna_reset', $data);
            }
            else
            {
                $this->index();
            }	
		}
		else
		{
			redirect('login','refresh');
		}
	}
}
