<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('message'))
{
	function message($msg="",$tipe="")
	{
		$str = '<div class="uk-alert uk-alert-'.$tipe.'"  data-uk-alert>
			<button class="uk-alert-close uk-close"></button>
			'.$msg.'
		</div>';
		return $str;
	}
}

?>