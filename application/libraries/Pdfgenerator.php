<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
require("system/libraries/dompdf-master/dompdf_config.inc.php");
class PdfGenerator
{
  public function generate($html,$filename,$orientation)
  { 
    $dompdf = new DOMPDF();
    $dompdf->load_html($html);
	$dompdf->set_paper('F4',$orientation);
    $dompdf->render();
    $dompdf->stream($filename.'.pdf',array("Attachment"=>0));	
  }

}