<?php
class Model_hakakses extends CI_Model
{	
	function get_hakakses_by_sgroupnama($key)
	{
		$qr = $this->db->query("
						SELECT 
						  * 
						FROM
						  s_user_group 
						WHERE sgroupNama ='".$key."'
						");

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}
	
	function edit_hakakses($key,$sgroupNama,$sgroupKeterangan)
	{
		$data = array(
               		'sgroupNama' => $sgroupNama,
               		'sgroupKeterangan' => $sgroupKeterangan
            	);
		$this->db->trans_start();
		$this->db->where('sgroupNama', $key);
		$this->db->update('s_user_group', $data); 
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
			return false;
		else
			return true;
	}
	
	function get_id_last_fak()
	{
		$qr = $this->db->query("
						SELECT fakKode FROM fakultas ORDER BY fakKode DESC LIMIT 1
						");

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}
	
	function tambah_hakakses($sgroupNama,$sgroupKeterangan)
	{
		$data = array(
               		'sgroupNama' => $sgroupNama,
               		'sgroupKeterangan' => $sgroupKeterangan
            	);
		
		$this->db->trans_start();
		$this->db->insert('s_user_group',$data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
			return false;
		else
			return true;			
	}
	
	public function hapus_hakakses($sgroupNama)
	{
		$this->db->trans_start();
		$this->db->where('sgroupNama', $sgroupNama);
		$this->db->delete('s_user_group');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
			return false;
		else
			return true;
	}

}
?>