<?php
class Model_hakaksesmodul extends CI_Model
{	
	function get_modul($sgroupNama)
	{
		$qr = $this->db->query("
						SELECT 
							susrmodulNama,
							susrmodulNamaDisplay,
							sgroupmodulSgroupNama,
							sgroupmodulSusrmodulNama  
						FROM
						  s_user_modul_ref a 
						  LEFT JOIN s_user_group_modul b 
							ON a.`susrmodulNama` = b.`sgroupmodulSusrmodulNama` 
							AND b.`sgroupmodulSgroupNama` = '".$sgroupNama."'  
						WHERE susrmodulIsLogin = 1 
						");

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}	
	
	
	public function hapus_modulakses($sgroupNama)
	{
		$this->db->trans_start();
		$this->db->where('sgroupmodulSgroupNama',$sgroupNama);
		$this->db->delete('s_user_group_modul');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
			return false;
		else
			return true;
	}

	function simpan_modulakses($sgroupNama,$modul)
	{
		$data = array(
               		'sgroupmodulSgroupNama' => $sgroupNama,
               		'sgroupmodulSusrmodulNama' => $modul,
					'sgroupmodulSusrmodulRead' => '1'
            	);
		
		$this->db->trans_start();
		$this->db->insert('s_user_group_modul',$data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
			return false;
		else
			return true;			
	}

}
?>