<?php
class Model_hakaksesunit extends CI_Model
{	
	function get_prodi($sgroupNama)
	{
		$qr = $this->db->query("
						SELECT 
						  prodiKode,
						  prodiNamaResmi,
						  prodiJjarKode,
						  sgroupunitSgroupNama,
						  sgroupunitUnitKode 
						FROM
						  program_studi 
						  LEFT JOIN s_user_group_unit
							ON prodikode = sgroupunitUnitKode 
							AND sgroupunitSgroupNama = '".$sgroupNama."'   
						ORDER BY prodiFakKode,prodiJjarKode 
						");

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}	
	
	
	public function hapus_aksesprodi($sgroupNama)
	{
		$this->db->trans_start();
		$this->db->where('sgroupunitSgroupNama',$sgroupNama);
		$this->db->delete('s_user_group_unit');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
			return false;
		else
			return true;
	}

	function simpan_aksesprodi($sgroupNama,$prodi)
	{
		$data = array(
               		'sgroupunitSgroupNama' => $sgroupNama,
               		'sgroupunitUnitKode' => $prodi
            	);
		
		$this->db->trans_start();
		$this->db->insert('s_user_group_unit',$data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
			return false;
		else
			return true;			
	}

}
?>