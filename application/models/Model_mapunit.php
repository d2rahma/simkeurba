<?php
class Model_mapunit extends CI_Model
{
	function get_prodi()
	{
		$qr = $this->db->query("
						SELECT 
						  prodiKode,
						  prodiNamaResmi,
						  prodiJjarKode,
						  PRODI_KODE_PARENT,
						  PRODI_NAMA_RESMI_PARENT 
						FROM
						  program_studi 
						  LEFT JOIN 
							(SELECT 
							  prodiKode AS PRODI_KODE_PARENT,
							  prodiNamaResmi AS PRODI_NAMA_RESMI_PARENT 
							FROM
							  program_studi) b 
							ON PRODI_KODE_PARENT = prodiParent  
						");

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}
	function get_prodi_by_prodikode($key)
	{
		$qr = $this->db->query("
						SELECT 
						  prodiKode,prodiNamaResmi,prodiJjarKode,prodiParent 
						FROM
						  program_studi 
						WHERE prodiKode ='".$key."'
						");

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}
	
	function get_prodi_by_prodiParent()
	{
		$qr = $this->db->query("
						SELECT 
						  prodiKode,prodiNamaResmi,prodiJjarKode 
						FROM
						  program_studi 
						WHERE prodiKode ='".$prodiParent."'
						");

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}
	
	function edit_mapunit($key,$prodiParent)
	{
		$data = array(
               		'prodiParent' => $prodiParent
            	);
		$this->db->trans_start();
		$this->db->where('prodiKode', $key);
		$this->db->update('program_studi', $data); 
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
			return false;
		else
			return true;
	}		
}
?>