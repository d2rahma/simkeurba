<?php
class Model_master extends CI_Model
{
	function otentifikasi_menu_by_susrSgroupNama($susrSgroupNama,$susrmodulNama)
	{
		$this->db->select('susrmodulNamaDisplay,susrmdgroupDisplay');
		$this->db->from('s_user_group_modul');
		$this->db->join('s_user_modul_ref','sgroupmodulSusrmodulNama=susrmodulNama','left');
		$this->db->join('s_user_modul_group_ref','susrmodulSusrmdgroupNama=susrmdgroupNama','left');
		$this->db->where('sgroupmodulSgroupNama', $susrSgroupNama);
		$this->db->where('sgroupmodulSusrmodulRead','1');
		$this->db->where('sgroupmodulSusrmodulNama', $susrmodulNama);
		$this->db->limit(1);
		$qr=$this->db->get();

		if($qr->num_rows()==1)
			return $qr->result();
		else
			return FALSE;
	}
	function otentifikasi_menu_by_susrNama($susrNama,$susrmodulNama)
	{
		$this->db->select('susrmodulNamaDisplay,susrmdgroupDisplay');
		$this->db->from('s_user');
		$this->db->join('s_user_group_modul','susrSgroupNama = sgroupmodulSgroupNama','left');
		$this->db->join('s_user_modul_ref','sgroupmodulSusrmodulNama=susrmodulNama','left');
		$this->db->join('s_user_modul_group_ref','susrmodulSusrmdgroupNama=susrmdgroupNama','left');
		$this->db->where('susrNama', $susrNama);
		$this->db->where('sgroupmodulSusrmodulRead','1');
		$this->db->where('sgroupmodulSusrmodulNama', $susrmodulNama);
		$this->db->limit(1);
		$qr=$this->db->get();

		if($qr->num_rows()==1)
			return $qr->result();
		else
			return FALSE;
	}
	
	function get_hakakses()
	{
		$qr = $this->db->query("
						SELECT sgroupNama,sgroupKeterangan
						FROM s_user_group 
						");

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}
	
	function jenis_pembayaran()
	{
	$qr = $this->db->query("
						SELECT * FROM b_jenis_pembayaran
						");

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}
	function createBillNum($bmtKodePembayaran,$mhsNiu,$PNim)
	{
		$this->load->helper("desimal");
		if(trim($PNim)==14)
			$bill_num = $bmtKodePembayaran."0".substr($mhsNiu,0,7).substr($mhsNiu,-2,2);
		elseif(trim($PNim)==10)
			$bill_num = $bmtKodePembayaran.$mhsNiu;
		elseif(trim($PNim)<10)
		{
			$s = 10 - strlen($mhsNiu);
			$bill_num = $bmtKodePembayaran.substr("0000000000".$mhsNiu,-10);
		}
		elseif(trim($PNim)==17)
		{
			$s = explode('/',$mhsNiu);
			$bill_num = $bmtKodePembayaran."000".$s[0].$s[2].$s[3];	
		}
		elseif(trim($PNim)==16)
		{
			$s = explode('/',$mhsNiu);
			$bill_num = $bmtKodePembayaran."0".$s[0].$s[2].$s[3];	
		}
		elseif(trim($PNim)==22)
		{
			$s = explode('-',$mhsNiu);
			$bill_num = $bmtKodePembayaran."0".$s[2].desimal($s[1]).$s[3];	
		}
		elseif((trim($PNim)==15))
		{
			if (strstr($mhsNiu,'-'))
			{
				$s = explode('-',$mhsNiu);
				$bill_num = $bmtKodePembayaran."0".$s[2].desimal($s[1]).$s[3];	
			}
			elseif (strstr($mhsNiu,'/'))
			{
				$e=explode('/',$mhsNiu);
				$bill_num = $bmtKodePembayaran."000".$e[0].$e[2].$e[3];
			}
		}
		elseif(strstr($mhsNiu,'B'))			
		{
			$s = explode('B',$mhsNiu);
			$bill_num = $bmtKodePembayaran."00".$s[0].substr($s[1],0,3).substr($s[1],-2);	

		}
		
		return $bill_num;
	}
}
?>