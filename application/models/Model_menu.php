<?php
class Model_menu extends CI_Model
{
	function get_menu_by_susrSgroupNama($susrSgroupNama)
	{
		$this->db->select('susrmodulNama,susrmodulNamaDisplay,susrmdgroupDisplay,susrmdgroupIcon');
		$this->db->from('s_user_group_modul');
		$this->db->join('s_user_modul_ref','sgroupmodulSusrmodulNama=susrmodulNama','left');
		$this->db->join('s_user_modul_group_ref','susrmodulSusrmdgroupNama=susrmdgroupNama','left');
		$this->db->where('sgroupmodulSgroupNama', $susrSgroupNama);
		$this->db->where('sgroupmodulSusrmodulRead','1');
		$this->db->where('susrmodulSusrmdgroupNama IS NOT NULL',null,false);
		$this->db->order_by('susrmdgroupDisplay,susrmodulNamaDisplay');

		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}
	function get_menu_non_login()
	{
		$this->db->select('susrmodulNama,susrmodulNamaDisplay');
		$this->db->from('s_user_modul_ref');
		$this->db->where('susrmodulIsLogin','0');
		$this->db->order_by('susrmodulUrut');

		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}
}
?>