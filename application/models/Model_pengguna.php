<?php
class Model_pengguna extends CI_Model
{	
	function get_hakakses_by_id($sgroupNama)
	{
		$qr = $this->db->query("
						SELECT sgroupNama,sgroupKeterangan
						FROM s_user_group 
						WHERE sgroupNama='".$sgroupNama."'
						");

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}
	function get_pengguna($sgroupNama)
	{
		$qr = $this->db->query("
						SELECT 
						  susrNama,susrProfil,susrSgroupNama,susrPassword 
						FROM
						  s_user  
						WHERE susrSgroupNama = '".$sgroupNama."'
						");

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}
	
	function tambah_pengguna($susrNama,$susrPassword,$sgroupNama,$susrProfil)
	{
		$data = array(
					'susrNama' => $susrNama,
               		'susrPassword' => $susrPassword,
					'susrSgroupNama' => $sgroupNama,
               		'susrProfil' => $susrProfil
            	);
		$this->db->trans_start();
		$this->db->insert('s_user',$data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
			return false;
		else
			return true;			
	}
	
	function get_pengguna_by_id($susrNama)
	{
		$qr = $this->db->query("
						SELECT susrNama,susrProfil,susrSgroupNama
						FROM s_user 
						wHERE susrNama='".$susrNama."'
						");

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return false;
	}
	
	function edit_pengguna($susrNama,$susrProfil,$susrSgroupNama)
	{
		$data = array(
					'susrProfil' => $susrProfil,
               		'susrSgroupNama' => $susrSgroupNama
            	);
		$this->db->trans_start();
		$this->db->where('susrNama', $susrNama);
		$this->db->update('s_user', $data); 
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
			return false;
		else
			return true;
	}
	function hapus_pengguna($param) {
		$this->db->trans_start();
		$this->db->delete('s_user', $param);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return false;
		else
			return true;
	}
	function update_password($susrNama,$susrPassword)
	{
		$data = array(
                        'susrPassword' => MD5($susrPassword)
                );
                $this->db->trans_start();
                $this->db->where('susrNama', $susrNama);
                $this->db->update('s_user', $data);
                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE)
                        return false;
                else
                        return true;

	}
	
}
?>