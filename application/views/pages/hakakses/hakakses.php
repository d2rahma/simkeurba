<div id="response">     
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="uk-overflow-container">
            <div class="uk-width-medium-1-6">
                <a class="md-btn md-btn-primary md-btn-wave-light" href="<?=$add_url?>">Tambah</a>
            </div>                         
            <table class="uk-table uk-table-hover">
               <thead>
                <tr>
                    <th rowspan="2">
                         No.
                    </th>
                    <th rowspan="2">
                         Nama Hak Akses
                    </th>
                    <th rowspan="2">
                         Keterangan Hak Akses
                    </th>
                    <th rowspan="2">
                         Action
                    </th>
                </tr>
                </thead>
                 <tbody>                    
                    <?php
                        if($hakakses!==false) {
                            $i=1;
                            foreach($hakakses as $row)
                            {
                                $key = $this->encryptions->encode($row->sgroupNama,$this->config->item('encryption_key'));																							
                    ?>
                    <tr>
                        <td>
                             <?=$i?>
                        </td>
                        <td>
                             <?=$row->sgroupNama?>
                        </td>
                        <td>
                             <?=$row->sgroupKeterangan?>
                        </td>
                        <td>
                            <a href="<?=$edit_url.$key?>" title="Ubah" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}"><i class="md-icon material-icons uk-text-success">edit</i></a>
                            <a href="<?=$delete_url.$key;?>" class="ts_remove_row" title="Hapus" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}" id='ts_remove_row<?= $i; ?>'><i class="md-icon material-icons uk-text-danger">delete</i></a>
                                   
                        </td>                                    
                        <?php
                        
                                $i++;
                                    }
                        } else
                            echo "<td colspan='11'>Data Tidak Ditemukan</td>";
                    ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>   
</div>                               
