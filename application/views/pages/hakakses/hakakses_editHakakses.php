<?php
	$key = $this->encryptions->encode($sgroupNama,$this->config->item('encryption_key'));	
	$att = array('class'=>'uk-form-stacked','id'=>'form_hakakses');
	echo form_open(base_url().'hakakses/proses_edit_hakakses/'.$key,$att); 					
?>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
    <i class="md-icon material-icons">launch</i>  Ubah Hak Akses<hr />
        <div class="uk-grid">
			<div class="uk-width-medium-1-1">
            <div id="response"></div>
                <div class="parsley-row">
                    <label for="fullname">Nama Hak Akses<span class="req">*</span></label>
                    <input type="text" name="sgroupNama" id="sgroupNama" required class="md-input" value="<?=$sgroupNama?>"/>
                </div>                
			</div>
		</div>
		<div class="uk-grid">
			<div class="uk-width-medium-1-1">         
            	<div class="parsley-row">  
                    <label for="fullname">Keterangan Hak Akses<span class="req">*</span></label>
                    <input type="text" name="sgroupKeterangan" id="sgroupKeterangan" required class="md-input" value="<?=$sgroupKeterangan?>" />
                </div>
			</div>
		</div>        
        <div class="uk-grid">
            <div class="uk-width-medium-1-1">
                <button type="submit" class="md-btn md-btn-success md-btn-wave-light" id="btn">Simpan</button>
            </div>
        </div>  
    </div>
</div>