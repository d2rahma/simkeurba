<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
    <i class="md-icon material-icons">launch</i>  Tambah Hak Akses<hr />
    <?php                                      
        $att = array('class'=>'uk-form-stacked','id'=>'form_hakakses');
        echo form_open($save_url,$att); 
	?>							
        <div class="uk-grid">
			<div class="uk-width-medium-1-1">
            <div id="response"></div>
                <div class="parsley-row">
                    <label for="fullname">Nama Hak Akses<span class="req">*</span></label>
                    <input type="text" name="sgroupNama" id="sgroupNama" required class="md-input"/>
                </div>
			</div>
		</div>
		<div class="uk-grid">
			<div class="uk-width-medium-1-1">     
                <div class="parsley-row">      
                    <label for="fullname">Keterangan Hak Akses<span class="req">*</span></label>
                    <input type="text" name="sgroupKeterangan" id="sgroupKeterangan" required class="md-input"/>
                </div>
			</div>
		</div>        
        <div class="uk-grid">
            <div class="uk-width-medium-1-1">
                <button type="submit" class="md-btn md-btn-success md-btn-wave-light" id="btn">Simpan</button>
            </div>
        </div>  
    </div>
</div>
