<div class="md-card uk-margin-medium-bottom">
	<div class="md-card-content">
		<i class="md-icon material-icons">launch</i>  Data Modul Hak Akses<hr />   
		<?php
        if (isset($pesan))
        echo $pesan;
        $att = array('class'=>'uk-form-stacked', 'id'=>'form_response');
        echo form_open($simpan_url,$att); 
        ?>                 
        <div id="response"></div>
        <div class="uk-width-medium-1-6">
            <button type="submit" class="md-btn md-btn-warning md-btn-wave-light">Perbaharui</button>
        </div>
        <table class="uk-table uk-table-hover">
            <thead>
            <tr>
                <th rowspan="2">
                     <label><input type="checkbox" id="checkedAll"></label>
                </th>
                <th rowspan="2">
                     Nama Modul Akses
                </th>
                <th rowspan="2">
                     Keterangan Modul Akses
                </th>
            </tr>
            </thead>
            <tbody>            
            <input type="hidden" value="<?= $sgroupNama ?>" name='sgroupnama'/>
            <?php			
                                
                if($get_modul!==false) {
                    foreach($get_modul as $modul)
                    {			
                    if (!empty($modul->sgroupmodulSgroupNama))
                        $cek = "checked";
                     else
                        $cek=""; 
                                                                                                                                                                            
            ?>
            <tr>
                <td>
                    <input type="checkbox" class="checked" <?=$cek?> name="cekModul[]" value="<?=$modul->susrmodulNama?>"/>                                        
                </td>
                <td>
                     <?=$modul->susrmodulNama?>
                </td>
                <td>
                     <?=$modul->susrmodulNamaDisplay?>
                </td>                                                                
                <?php      
                            }
                } else
                    echo "<td colspan='11'>Data Tidak Ditemukan</td>";
            ?>
            </tbody>
            </table>
		</form>
    </div>
</div>