<?php 
	$att = array('class'=>'uk-form-stacked','id'=>'form_hakaksesunit');
	echo form_open(base_url().'hakaksesunit/tampil_aksesunit',$att);                              
?>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
    <?php echo validation_errors();?>    
        <div class="uk-grid">
            <div class="uk-width-medium-1-1">  
            	<div class="parsley-row">               
                    <select id="val_select" required data-md-selectize name="sgroupNama">
                        <option value="">Pilih Hak Akses</option>
                        <?php 
                        foreach($hakakses as $row)
                        {
                            echo '<option value="'.$row->sgroupNama.'">'.$row->sgroupNama.'</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>                    

        <div class="uk-grid">
            <div class="uk-width-medium-1-1">
                <button type="submit" class="md-btn md-btn-primary md-btn-wave-light" id="btn-tampil">Tampil</button>
            </div>
        </div>
	</div>
</div>
</form>
<div id="response"></div>

  						