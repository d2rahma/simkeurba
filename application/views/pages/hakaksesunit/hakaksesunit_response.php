<div class="md-card uk-margin-medium-bottom">
	<div class="md-card-content">
		<i class="md-icon material-icons">launch</i>  Data Modul Hak Akses Unit<hr />   
		<?php
        if (isset($pesan))
        echo $pesan;
        $att = array('class'=>'uk-form-stacked', 'id'=>'form_response_hakaksesunit');
        echo form_open($simpan_url,$att); 
        ?>                 
        <div id="response"></div>
        <div class="uk-width-medium-1-6">
            <button type="submit" class="md-btn md-btn-warning md-btn-wave-light">Perbaharui</button>
        </div>
        <table class="uk-table uk-table-hover">
            <thead>
             <tr>
                <th rowspan="2">
                     <label><input type="checkbox" id="checkedAll"></label>
                </th>
                <th rowspan="2">
                     Kode Unit
                </th>
                <th rowspan="2">
                     Nama Unit
                </th>
                <th rowspan="2">
                     Jenjang Prodi
                </th>
                <th rowspan="2">
                     Hak Akses
                </th>
            </tr>
            </thead>
            <tbody>            
            <input type="hidden" value="<?= $sgroupNama ?>" name='sgroupnama'/>
            <?php			
			$no=1;                                
                if($get_prodi!==false) {
					foreach($get_prodi as $prodi)
				{			
				if (!empty($prodi->sgroupunitSgroupNama))
					$cek = "checked";
				 else
					$cek=""; 
                                                                                                                                                                            
            ?>
            <tr>
                <td>
                    <input type="checkbox" class="checked" <?=$cek?> name="cekProdi[]" value="<?=$prodi->prodiKode?>"/>                                        
                </td>
                <td>
                    <?=$no;?>                                                                          
                </td>
                <td>
                     <?=$prodi->prodiNamaResmi?>
                </td>               
                <td>
					 <?=$prodi->prodiJjarKode?>
                </td>
                 <td>
                     <?=$prodi->sgroupunitSgroupNama?>
                </td>                                                                
                <?php      
                $no++;
                            }
                            
                } else
                    echo "<td colspan='11'>Data Tidak Ditemukan</td>";
            ?>
            </tbody>
            </table>
		</form>
    </div>
</div>