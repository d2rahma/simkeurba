<!-- BEGIN PAGE HEADER-->
<h3 class="page-title"><?php echo $judul;?></h3>

<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="<?=base_url();?>">Home</a>
		</li>
		
		<?php if (isset($status_page)) :?>
		<li>
			<i class="fa fa-angle-right"></i>
			<a href="#"><?php echo $status_page; ?></a>
		</li>
		<?php endif;?>
	</ul>
	<?php
	if(isset($semAktif)) {
	?>
	<div class="page-toolbar">
		<div class="pull-right tooltips btn btn-sm btn-default">
			<i class="glyphicon glyphicon-calendar"></i>&nbsp; Semester Aktif - <?=$semAktif?>
		</div>
	</div>
	<?php
	}
	?>
</div>
<!-- END PAGE HEADER-->
