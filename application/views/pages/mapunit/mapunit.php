<div id="response">
<?php
if (isset($pesan))
echo $pesan;
?>      
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
                         
        <table class="uk-table uk-table-hover">
           <thead>
            <tr>
                <th>
                     No.
                </th>
                <th>
                     Nama Unit
                </th>
                <th>
                     Parent Unit
                </th>
                <th>
                     Jenjang Unit
                </th>
                <th>
                     Action
                </th>
            </tr>
            </thead>
             <tbody>                    
                <?php
                    if($mapunit!==false) {
                        $i=1;
                        foreach($mapunit as $row)
                        {
                            $key = $this->encryptions->encode($row->prodiKode,$this->config->item('encryption_key'));																							
                ?>
                <tr>
                    <td>
                         <?=$i?>
                    </td>
                    <td width="45%">
                         <?=$row->prodiNamaResmi?>
                    </td>
                    <td>
                         <?=$row->PRODI_NAMA_RESMI_PARENT?>
                    </td>
                    <td>
                         <?=$row->prodiJjarKode?>
                    </td>
                    <td>
                        <a href="<?=$edit_url.$key?>" title="Ubah" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}"><i class="md-icon material-icons uk-text-success">edit</i></a>                                                     
                    </td>                                    
                    <?php
                    
                            $i++;
                                }
                    } else
                        echo "<td colspan='11'>Data Tidak Ditemukan</td>";
                ?>
                </tr>
            </tbody>
        </table>
    </div>
</div>   
</div>                               
