<?php
	$key = $this->encryptions->encode($prodiKode,$this->config->item('encryption_key'));	
	$att = array('class'=>'uk-form-stacked','id'=>'form_mapunit');
	echo form_open(base_url().'mapunit/proses_edit_mapunit/'.$key,$att); 					
?>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
    <i class="md-icon material-icons">launch</i>  Ubah Mapping Unit <hr />
        <div class="uk-grid">
			<div class="uk-width-medium-1-1">
            <div id="response"></div>
                <div class="parsley-row">
                    <label for="fullname">Nama Unit<span class="req">*</span></label>
                    <input type="text" name="prodiNamaResmi" id="prodiNamaResmi" required class="md-input" value="<?=$prodiNamaResmi." - ".$prodiJjarKode?>" disabled="disabled"/>
                </div>                
			</div>
		</div>
		<div class="uk-grid">
			<div class="uk-width-medium-1-1">         
            	<div class="parsley-row">
                    <label for="fullname">Parent Unit</label>
                    <select id="val_select" data-md-selectize name="prodiParent" required>
                        <option value="">Pilih Parent Unit</option>
                        <?php 
                        foreach($mapunit as $row)
                        {
                            if ($row->prodiKode==$prodiParent)
                                echo "<option value='".$row->prodiKode."' selected='selected'>".$row->prodiNamaResmi." - ".$row->prodiJjarKode."</option>";
                            else
                                echo '<option value="'.$row->prodiKode.'">'.$row->prodiNamaResmi." - ".$row->prodiJjarKode.'</option>';
                        }
                        ?>
                    </select>
                </div>
			</div>
		</div>        
        <div class="uk-grid">
            <div class="uk-width-medium-1-1">
                <button type="submit" class="md-btn md-btn-success md-btn-wave-light" id="btn">Simpan</button>
            </div>
        </div>  
    </div>
</div>