<div class="page-content-wrapper">
	<div class="page-content">
		<?php $this->load->view('pages/menu/header')?>
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="portlet box blue">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i>Form <?=$status_page?> <?=$judul?>
						</div>
					</div>
					<div class="portlet-body form">
                    <div id='response'></div>
                    <?php                        
					$att = array('class'=>'form-horizontal','role'=>'form','id'=>'form-content');
					echo form_open($save_url,$att); 		
										
					?>							
							<div class="form-body">
								<?php echo validation_errors();?>
								<div class="alert alert-danger display-hide">
									<button class="close" data-close="alert"></button>
									You have some form errors. Please check below.
								</div>
								<div class="alert alert-success display-hide">
									<button class="close" data-close="alert"></button>
									Your form validation is successful!
								</div>
								<input type="hidden" name="cSusrmodulNama" value="<?=$cSusrmodulNama?>">
								<div class="form-group">
									<label class="col-md-2 control-label">Judul</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="cJudul" id="cJudul" value="<?=$datas!==false?$datas->cJudul:$cJudul?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">Content</label>
									<div class="col-md-8">
										<textarea class="form-control summernote" rows="21" name="cIsi" id="cIsi"><?=$datas!==false?$datas->cIsi:''?></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">Tag</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="cTag" id="cTag" value="<?=$datas!==false?$datas->cTag:''?>">
									</div>
								</div>
                                <div class="form-actions">
								<div class="row">
									<div class="col-md-offset-2 col-md-9">
										<button type="submit" class="btn green" id="btn-tampil">Simpan</button>
										<i class="fa fa-spinner fa-spin" id="animasi-loading"></i>
									</div>
								</div>
							</div>
							</div>
					</div>
				</div>					
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>