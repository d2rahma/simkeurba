<!-- BEGIN PAGE HEADER-->
<h3 class="page-title"><?php echo $judul;?></h3>

<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="<?=base_url();?>">Home</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#"><?=$breadcrumb->susrmdgroupDisplay?></a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#"><?=$breadcrumb->susrmodulNamaDisplay?></a>
			<?php echo (isset($status_page)) ? '<i class="fa fa-angle-right"></i>' : ''?>
		</li>
		
		<?php if (isset($status_page)) :?>
		<li>
			<a href="#"><?php echo $status_page; ?></a>
		</li>
		<?php endif;?>

	</ul>
</div>
<!-- END PAGE HEADER-->
