<div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Data <?=$judul;?>
                            </div>
                        </div>
                       
                        <div class="portlet-body">      
                        <?php
                        if (isset($pesan))
                        echo $pesan;
                        ?>                                        
                            <div class="table-responsive">                            
                                <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th rowspan="2">
                                         No.
                                    </th>
                                    <th rowspan="2">
                                         Menu
                                    </th>
                                    <th rowspan="2">
                                         Urutan (Kiri-Kanan)
                                    </th>
                                    <th rowspan="2">
                                         Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <div class="form-actions top">
                                     <a href="<?=$add_url?>" class="btn green">
                                            Tambah </a><hr />
                                </div>
								<?php
                                    if($menu_frontend!==false) {
                                        $i=1;
										foreach($menu_frontend as $row)
										{
											$key = $this->encryptions->encode($row->susrmodulNama,$this->config->item('encryption_key'));																							
                                ?>
                                <tr>
                                    <td>
                                         <?=$i?>
                                    </td>
                                    <td>
                                         <?=$row->susrmodulNamaDisplay?>
                                    </td>
                                    <td>
                                         <?=$row->susrmodulUrut?>
                                    </td>
                                    <td>
                                         <a href="<?=$edit_url.$key?>" class="btn default btn-xs purple">
                                            <i class="fa fa-edit"></i> Ubah 
                                         </a>
                                         <a href="<?php echo $delete_url.$key; ?>" class="btn default btn-xs red btn-delete hapus" id='hapus<?= $i; ?>'>
                                            <i class="fa fa-trash"></i> Hapus 
                                         </a>
                                         <a href="<?php echo $content_url.$key; ?>" class="btn default btn-xs yellow">
                                            <i class="fa icon-docs"></i> Content 
                                         </a>   
                                    </td>                                    
                                    <?php
                                    
                                            $i++;
												}
                                    } else
                                        echo "<td colspan='11'>Data Tidak Ditemukan</td>";
                                ?>
                                </tbody>
                                </table>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->