<div class="page-content-wrapper">
	<div class="page-content">
		<?php $this->load->view('pages/frontend/header')?>
		<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12 blog-page">
					<div class="row">
						<div class="col-md-9 col-sm-8 article-block">
							<div class="blog-tag-data">
								<div class="row">
									<div class="col-md-10">
										<ul class="list-inline blog-tags">
											<li>
												<i class="fa fa-tags"></i>
												<?php
												if($datas!==false) {
													$data = explode(',',$datas->cTag);
													foreach($data as $row) {
												?>
												<a href="javascript:;">
												<?=$row?> </a>
												<?php } } ?>
											</li>
										</ul>
									</div>
									<div class="col-md-2 blog-tag-data-inner">
										<ul class="list-inline">
											<li>
												<i class="fa fa-calendar"></i>
												<a href="javascript:;">
												<?=$datas!==false?date('F d, Y', strtotime($datas->cDatetime)):''?></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<!--end news-tag-data-->
							<div>
								<?=$datas!==false?$datas->cIsi:''?></a>
							</div>
						</div>
						<!--end col-md-9-->
						<div class="col-md-3 col-sm-4 blog-sidebar">
							<h3>Top Views</h3>
							<div class="top-news">
								<a href="javascript:;" class="btn red">
								<span>
								Metronic News </span>
								<em>Posted on: April 16, 2013</em>
								<em>
								<i class="fa fa-tags"></i>
								Money, Business, Google </em>
								<i class="fa fa-briefcase top-news-icon"></i>
								</a>
								<a href="javascript:;" class="btn green">
								<span>
								Top Week </span>
								<em>Posted on: April 15, 2013</em>
								<em>
								<i class="fa fa-tags"></i>
								Internet, Music, People </em>
								<i class="fa fa-music top-news-icon"></i>
								</a>
								<a href="javascript:;" class="btn blue">
								<span>
								Gold Price Falls </span>
								<em>Posted on: April 14, 2013</em>
								<em>
								<i class="fa fa-tags"></i>
								USA, Business, Apple </em>
								<i class="fa fa-globe top-news-icon"></i>
								</a>
								<a href="javascript:;" class="btn yellow">
								<span>
								Study Abroad </span>
								<em>Posted on: April 13, 2013</em>
								<em>
								<i class="fa fa-tags"></i>
								Education, Students, Canada </em>
								<i class="fa fa-book top-news-icon"></i>
								</a>
								<a href="javascript:;" class="btn purple">
								<span>
								Top Destinations </span>
								<em>Posted on: April 12, 2013</em>
								<em>
								<i class="fa fa-tags"></i>
								Places, Internet, Google Map </em>
								<i class="fa fa-bolt top-news-icon"></i>
								</a>
							</div>							
						</div>
						<!--end col-md-3-->
					</div>					
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	</div>
</div>