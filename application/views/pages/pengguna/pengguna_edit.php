<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
    <i class="md-icon material-icons">launch</i>  Ubah Pengguna<hr />
    <?php                        
	$key = $this->encryptions->encode($susrNama,$this->config->item('encryption_key'));
	$att = array('class'=>'uk-form-stacked','id'=>'form_pengguna_action');
	echo form_open(base_url().'pengguna/proses_edit_pengguna/'.$key,$att); 						
	?>							
    <div id="response"></div>
        <div class="uk-grid">
			<div class="uk-width-medium-1-1">
                <div class="parsley-row">
                    <label for="fullname">Pengguna<span class="req">*</span></label>
                    <input type="text" name="susrNama" id="susrNama" required class="md-input" value="<?=$susrNama?>" disabled />
                </div>
			</div>
		</div>
        <div class="uk-grid">
			<div class="uk-width-medium-1-1">
                <div class="parsley-row">
                    <label for="fullname">Profil Pengguna<span class="req">*</span></label>
                    <input type="text" name="susrProfil" id="susrProfil" required class="md-input" value="<?=$susrProfil?>"/>
                </div>
			</div>
		</div>
        <div class="uk-grid">
			<div class="uk-width-medium-1-1">
                <div class="parsley-row">
                    <label for="fullname">Grup Pengguna<span class="req">*</span></label>
                    <select id="val_select" required data-md-selectize name="susrSgroupNama">
                        <option value="">Pilih Grup</option>
                        <?php 
                        foreach($hakakses as $row)
                        {
                            if ($row->sgroupNama==$susrSgroupNama)
                                echo "<option value='".$row->sgroupNama."' selected='selected'>".$row->sgroupNama."</option>";
                            else
                                echo '<option value="'.$row->sgroupNama.'">'.$row->sgroupNama.'</option>';
                        }
                        ?>
                    </select>
                </div>
			</div>
		</div>
        <div class="uk-grid">
            <div class="uk-width-medium-1-1">
                <button type="submit" class="md-btn md-btn-success md-btn-wave-light" id="btn">Simpan</button>
            </div>
        </div>  
    </div>
</div>