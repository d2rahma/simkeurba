<?php
$this->load->helper("generate");
?>
<div id="response">
<div class="md-card uk-margin-medium-bottom">
	<div class="md-card-content">
		<i class="md-icon material-icons">launch</i>  Data Modul Pengguna<hr />       
		<?php
	       	$id = $this->encryptions->encode($sgroupNama,$this->config->item('encryption_key'));
        ?>                                        
		<div class="table-responsive">
        	<a class="md-btn md-btn-primary md-btn-wave-light" href="<?=$add_url.$id?>">Tambah</a>
			<table class="uk-table uk-table-hover">
            	<thead>
                	<tr>
                        <th>
                             No.
                        </th>
                        <th>
                             Pengguna
                        </th>
                        <th>
                             Profil Pengguna
                        </th>
                        <th>
                             Password
                        </th>
                        <th>
                             Grup Pengguna
                        </th>
                        <th>
                             Action
                        </th>
                    </tr>
				</thead>
				<tbody>
                <?php
                    if($pengguna!==false) {
                        $i=1;
                        foreach($pengguna as $row)
                        {
                            $key = $this->encryptions->encode($row->susrNama.";".$sgroupNama,$this->config->item('encryption_key'));																							
                ?>
                    <tr>
                        <td>
                             <?=$i?>
                        </td>
                        <td>
                             <?=$row->susrNama?>
                        </td>
                        <td>
                             <?=$row->susrProfil?>
                        </td>
                        <td>
                             <?=$row->susrPassword?>
                        </td>
                         <td>
                             <?=$row->susrSgroupNama?>
                        </td>
                        <td>
							<a href="<?=$reset.$key?>" class="reset_pengguna" title="Reset Password" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}" id='reset_pengguna<?= $i; ?>'><i class="md-icon material-icons uk-text-warning">cached</i></a>                        
							<a href="<?=$edit_url.$key?>" title="Ubah" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}"><i class="md-icon material-icons uk-text-success">edit</i></a>
    	                    <a href="<?=$del_url.$key;?>" class="ts_remove_row" title="Hapus" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}" id='ts_remove_row<?= $i; ?>'><i class="md-icon material-icons uk-text-danger">delete</i></a>
                        </td>                                    
                        <?php
                        
                                $i++;
                                    }
                        } else
                            echo "<td colspan='11'>Data Tidak Ditemukan</td>";
                    ?>
                    </tr>
                </tbody>
            </table>	        
    	</div>
    </div>
</div>
</div>