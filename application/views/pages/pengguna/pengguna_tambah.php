<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
    <i class="md-icon material-icons">launch</i>  Tambah Pengguna<hr />
    <?php                                      
        $att = array('class'=>'uk-form-stacked','id'=>'form_pengguna_action');
        echo form_open(base_url().'pengguna/proses_tambah_pengguna/'.$sgroupNama,$att); 
	?>				
    <div id="response"></div>			
        <div class="uk-grid">
			<div class="uk-width-medium-1-1">
                <div class="parsley-row">
                    <label for="fullname">Pengguna<span class="req">*</span></label>
                    <input type="text" name="susrNama" id="susrNama" required class="md-input"/>
                </div>
			</div>
		</div>
        <div class="uk-grid">
			<div class="uk-width-medium-1-1">
                <div class="parsley-row">
                    <label for="fullname">Profil Pengguna<span class="req">*</span></label>
                    <input type="text" name="susrProfil" id="susrProfil" required class="md-input"/>
                </div>
			</div>
		</div>
        <div class="uk-grid">
			<div class="uk-width-medium-1-1">
            <div id="response"></div>
                <div class="parsley-row">
                    <label for="fullname">Password<span class="req">*</span></label>
                    <input type="text" name="susrPassword" id="susrPassword" value="<?= $password; ?>" readonly="readonly"class="md-input"/>
                </div>
			</div>
		</div>
        <div class="uk-grid">
            <div class="uk-width-medium-1-1">
                <button type="submit" class="md-btn md-btn-success md-btn-wave-light" id="btn">Simpan</button>
            </div>
        </div>  
    </div>
</div>
