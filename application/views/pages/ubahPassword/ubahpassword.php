<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
    <i class="md-icon material-icons">launch</i>  Ubah Password<hr />
    <?php										
	$key = $this->encryptions->encode($susrNama,$this->config->item('encryption_key'));
	$att = array('class'=>'uk-form-stacked','id'=>'form_edit');
	echo form_open(base_url().'home/proses_ubah_password/'.$key,$att);  						
	?>							
    <div id="response"></div>
        <div class="uk-grid">
			<div class="uk-width-medium-1-1">
                <div class="parsley-row">
                    <label for="fullname">Pengguna<span class="req">*</span></label>
                    <input type="text" name="susrNama" id="susrNama" required class="md-input" value="<?=$susrNama?>" disabled />
                </div>
			</div>
		</div>
        <div class="uk-grid">
			<div class="uk-width-medium-1-1">
                <div class="parsley-row">
                    <label for="fullname">Profil Pengguna<span class="req">*</span></label>
                    <input type="text" name="susrProfil" id="susrProfil" required class="md-input" value="<?=$susrProfil?>" disabled/>
                </div>
			</div>
		</div>
        <div class="uk-grid">
			<div class="uk-width-medium-1-1">
                <div class="parsley-row">
                    <label for="fullname">Grup Pengguna<span class="req">*</span></label>
                    <input type="text" name="susrProfil" id="susrProfil" required class="md-input" value="<?=$susrSgroupNama?>" disabled/>
                </div>
			</div>
		</div>
        <div class="uk-grid">
			<div class="uk-width-medium-1-1">
                <div class="parsley-row">
                    <label for="fullname">Password Lama<span class="req">*</span></label>
                    <input type="password" name="susrPasswordLama" id="susrPasswordLama" required class="md-input"/>
                </div>
			</div>
		</div> 
        <div class="uk-grid">
			<div class="uk-width-medium-1-1">
                <div class="parsley-row">
                    <label for="fullname">Password Baru<span class="req">*</span></label>
                    <input type="password" name="susrPassword" id="susrPassword" required class="md-input"/>
                </div>
			</div>
		</div> 
        <div class="uk-grid">
			<div class="uk-width-medium-1-1">
                <div class="parsley-row">
                    <label for="fullname">Konfirmasi Password Baru<span class="req">*</span></label>
                    <input type="password" name="susrPasswordKonfirm" id="susrPasswordKonfirm" required class="md-input" data-parsley-equalto="#susrPassword"/>
                </div>
			</div>
		</div>        
        <div class="uk-grid">
            <div class="uk-width-medium-1-1">
                <button type="submit" class="md-btn md-btn-success md-btn-wave-light" id="btn">Simpan</button>
            </div>
        </div>  
    </div>
</div>    