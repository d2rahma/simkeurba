<aside id="sidebar_main">   
    <div class="sidebar_main_header">
        <div class="sidebar_logo">
            <a href="index.html" class="sSidebar_hide sidebar_logo_large">
                <img class="logo_regular" src="<?php echo base_url(); ?>public/assets/img/logo_main.png" alt="" height="80" width="200"/>
                <img class="logo_light" src="<?php echo base_url(); ?>public/assets/img/logo_main_white.png" alt="" height="80" width="200"/>
            </a>
            <a href="index.html" class="sSidebar_show sidebar_logo_small">
                <img class="logo_regular" src="<?php echo base_url(); ?>public/assets/img/logo_main_small.png" alt="" height="32" width="32"/>
                <img class="logo_light" src="<?php echo base_url(); ?>public/assets/img/logo_main_small_light.png" alt="" height="32" width="32"/>
            </a>
        </div>
    </div>   
    
    <div class="menu_section">			
    <ul>
    	<li title="Dashboard">
            <a href="<?php echo base_url(); ?>home">
                <span class="menu_icon"><i class="material-icons">home</i></span>
                <span class="menu_title">Dashboard</span>
            </a>            
        </li>
	</ul>
	<?php
    $curMenu = '';
    $cRow = 1;
    $susrmdgroupDisplay = '';
    $susrmodulNamaDisplay = '';


    if(isset($breadcrumb)) {
        $susrmdgroupDisplay = $breadcrumb->susrmdgroupDisplay;
        $susrmodulNamaDisplay = $breadcrumb->susrmodulNamaDisplay;
    }


    foreach($menu as $row)
    {
        if($curMenu!=$row->susrmdgroupDisplay and $cRow==1) 
		{
            $curMenu=$row->susrmdgroupDisplay;
    ?>
            <ul>        
                <li title="<?=$susrmdgroupDisplay?>" class="<?=$row->susrmdgroupDisplay==$susrmdgroupDisplay ? 'current_section' : ''?>">                       
                    <a href="javascript:;">
                        <span class="menu_icon"><i class="material-icons"><?=$row->susrmdgroupIcon?></i></span>
                        <span class="menu_title"><?=$row->susrmdgroupDisplay?></span>								
                    </a>
                    <ul>
			<?php
		} elseif($curMenu!=$row->susrmdgroupDisplay and $cRow!=1) {
			$curMenu=$row->susrmdgroupDisplay;
			?>
            </ul>
        	</li>
			<li title="<?=$susrmdgroupDisplay?>" class="<?=$row->susrmdgroupDisplay==$susrmdgroupDisplay ? 'current_section' : ''?>">
				<a href="javascript:;">
					 <span class="menu_icon"><i class="material-icons"><?=$row->susrmdgroupIcon?></i></span>
					<span class="menu_title"><?=$row->susrmdgroupDisplay?></span>				
				</a>
				<ul>
			<?php
			}
			?>
					<li class="<?=$row->susrmodulNamaDisplay==$susrmodulNamaDisplay ? 'act_item' : ''?>">
						<a href="/<?=$row->susrmodulNama?>">
						<i class="uk-icon-minus-square-o"></i>
						<?=$row->susrmodulNamaDisplay?></a>
					</li>
			<?php
				$cRow++;
			}
			?>
				</ul>
			</li>
		</ul>        
    </div>
</aside>
