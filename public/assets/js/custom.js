/**
Custom module for you to write your own javascript functions
**/
var Custom = function () {

    // private functions & variables

    var checkedAllCheckbox = function() {
        var checkId = $('#checkedAll');
        var checkClass = $('.checked');

        $(checkId).click(function () {
            $(checkClass).prop('checked', $(this).prop('checked'));
        });
    }

    // public functions
    return {

        //main function
        init: function () {
            //initialize here something. 
            checkedAllCheckbox();           
        },

        //some helper function
        doSomeStuff: function () {
            //your function
        }

    };

}();

/***
Usage
***/
//Custom.init();
//Custom.doSomeStuff();