$(function() {
    // validation
    FormValidation.init();
});

var FormValidation = function () {
    var $formValidate = $('#form_hakaksesunit');
    var $formResponse = $('#form_response_hakaksesunit');
	
	var $loading = $('#loading-spinner');
	var $btnTampil = $('#btn-tampil');

    var form_hakaksesunit = function() {
		var $formValidate = $('#form_hakaksesunit');
         $formValidate
            .parsley()
            .on('form:validated',function() {
                altair_md.update_input($formValidate.find('.md-input-danger'));
            })
            .on('field:validated',function(parsleyField) {
                if($(parsleyField.$element).hasClass('md-input')) {
                    altair_md.update_input( $(parsleyField.$element) );
                }
            });

        window.Parsley.on('field:validate', function() {
            var $server_side_error = $(this.$element).closest('.md-input-wrapper').siblings('.error_server_side');
            if($server_side_error) {
                $server_side_error.hide();
            }
        });

        $formValidate.submit(function(e) { 
			var className = $btnTampil.attr('class');
			$loading.show();
			$btnTampil.html('Sedang Diproses <i class="uk-icon-spinner uk-icon-small uk-icon-spin" id="loading-spinner"></i>');
			$btnTampil.prop('class', 'md-btn disabled');
			
            e.preventDefault();
            if ( $(this).parsley().isValid() ) {
                $.ajax({
                    type: $formValidate.attr('method'),
                    url: $formValidate.attr('action'),
                    data: $formValidate.serialize(),
                    success: function(data) {                        
                        $('#response').html(data);   
						$btnTampil.prop('class', className);						
						$btnTampil.html('Tampil');
						$loading.hide();
						form_response_hakaksesunit();						   
						Custom.init(); 
                    }
                })
                return false;       
            }
        });
    }

    var form_response_hakaksesunit = function() {
		 var $formResponse = $('#form_response_hakaksesunit');
         $formResponse
            .parsley()
            .on('form:validated',function() {
                altair_md.update_input($formResponse.find('.md-input-danger'));
            })
            .on('field:validated',function(parsleyField) {
                if($(parsleyField.$element).hasClass('md-input')) {
                    altair_md.update_input( $(parsleyField.$element) );
                }
            });

        window.Parsley.on('field:validate', function() {
            var $server_side_error = $(this.$element).closest('.md-input-wrapper').siblings('.error_server_side');
            if($server_side_error) {
                $server_side_error.hide();
            }
        });

        $formResponse.submit(function(e) { 
		var atLeastOneIsChecked = $('input[name="cekProdi[]"]:checked').length > 0;

            e.preventDefault();
			
            if ( $(this).parsley().isValid() ) {
				if(atLeastOneIsChecked)
				{	
					UIkit.modal.confirm('Apakah Data yang Akan Diperbaharui Sudah Benar?', 
					function(){ 
					 $.ajax({
                    type: $formResponse.attr('method'),
                    url: $formResponse.attr('action'),
                    data: $formResponse.serialize(),
                    success: function(data) {
                       $('#response').html(data);              
                    }
					})
					});   
				   
					return false;       
				}
				else
				UIkit.modal.alert('Pilih Minimal 1 Data Unit Hak Akses');
            }
        });
    }


    return {
        //main function to initiate the module
        init: function () {
            if($formValidate.length>0) form_hakaksesunit();
        }
    };
}();
