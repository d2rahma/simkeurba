$(function() {
    // validation
    FormValidation.init();
});

var FormValidation = function () {
    var $formValidate = $('#form_datamahasiswa');
	var $loading = $('#loading-spinner');
	var $btnTampil = $('#btn-tampil');

    var form_datamahasiswa = function() {
         $formValidate
            .parsley()
            .on('form:validated',function() {
                altair_md.update_input($formValidate.find('.md-input-danger'));
            })
            .on('field:validated',function(parsleyField) {
                if($(parsleyField.$element).hasClass('md-input')) {
                    altair_md.update_input( $(parsleyField.$element) );
                }
            });

        window.Parsley.on('field:validate', function() {
            var $server_side_error = $(this.$element).closest('.md-input-wrapper').siblings('.error_server_side');
            if($server_side_error) {
                $server_side_error.hide();
            }
        });

        $formValidate.submit(function(e) { 
			var className = $btnTampil.attr('class');
			$loading.show();
			$btnTampil.html('Sedang Diproses <i class="uk-icon-spinner uk-icon-small uk-icon-spin" id="loading-spinner"></i>');
			$btnTampil.prop('class', 'md-btn disabled');
            e.preventDefault();
            if ( $(this).parsley().isValid() ) {
                $.ajax({
                    type: $formValidate.attr('method'),
                    url: $formValidate.attr('action'),
                    data: $formValidate.serialize(),
                    success: function(data) {                        
                        $('#response').html(data);						  
						$btnTampil.prop('class', 'md-btn md-btn-primary md-btn-wave-light');						
						$btnTampil.html('Tampil');
						$loading.hide(); 
                    }
                })
                return false;       
            }
        });
    }
    	
    return {
        //main function to initiate the module
        init: function () {
            if($formValidate.length>0) form_datamahasiswa();
        }
    };
}();
