$(function() {
    // validation
    FormValidation.init();
});

var FormValidation = function () {
    var $formValidate = $('#form_mastertagihan');
	var $formValidateAction = $('#form_mastertagihan_action');
	var $link = $('.ts_remove_row');
	
	var $loading = $('#loading-spinner');
	var $btnTampil = $('#btn-tampil');
	var $btn = $('#btn');

    var form_mastertagihan = function() {
         $formValidate
            .parsley()
            .on('form:validated',function() {
                altair_md.update_input($formValidate.find('.md-input-danger'));
            })
            .on('field:validated',function(parsleyField) {
                if($(parsleyField.$element).hasClass('md-input')) {
                    altair_md.update_input( $(parsleyField.$element) );
                }
            });

        window.Parsley.on('field:validate', function() {
            var $server_side_error = $(this.$element).closest('.md-input-wrapper').siblings('.error_server_side');
            if($server_side_error) {
                $server_side_error.hide();
            }
        });

        $formValidate.submit(function(e) { 
			var className = $btnTampil.attr('class');
			$loading.show();
			$btnTampil.html('Sedang Diproses <i class="uk-icon-spinner uk-icon-small uk-icon-spin" id="loading-spinner"></i>');
			$btnTampil.prop('class', 'md-btn disabled');
            e.preventDefault();
            if ( $(this).parsley().isValid() ) {
                $.ajax({
                    type: $formValidate.attr('method'),
                    url: $formValidate.attr('action'),
                    data: $formValidate.serialize(),
                    success: function(data) {                        
                        $('#response').html(data);   
						hapus();  
						$btnTampil.prop('class', 'md-btn md-btn-primary md-btn-wave-light');						
						$btnTampil.html('Tampil');
						$loading.hide();
                    }
                })
                return false;       
            }
        });
    }    	
	
	
	var form_mastertagihan_action = function() {
         $formValidateAction
            .parsley()
            .on('form:validated',function() {
                altair_md.update_input($formValidate.find('.md-input-danger'));
            })
            .on('field:validated',function(parsleyField) {
                if($(parsleyField.$element).hasClass('md-input')) {
                    altair_md.update_input( $(parsleyField.$element) );
                }
            });

        window.Parsley.on('field:validate', function() {
            var $server_side_error = $(this.$element).closest('.md-input-wrapper').siblings('.error_server_side');
            if($server_side_error) {
                $server_side_error.hide();
            }
        });

        $formValidateAction.submit(function(e) { 
			var className = $btn.attr('class');
			$loading.show();
			$btn.html('Sedang Diproses <i class="uk-icon-spinner uk-icon-small uk-icon-spin" id="loading-spinner"></i>');
			$btn.prop('class', 'md-btn disabled');
            e.preventDefault();
            if ( $(this).parsley().isValid() ) {
                $.ajax({
                    type: $formValidateAction.attr('method'),
                    url: $formValidateAction.attr('action'),
                    data: $formValidateAction.serialize(),
                    success: function(data) {
                        $('#response').html(data);    
						$btn.prop('class', className);						
						$btn.html('Simpan');
						$loading.hide();      
                    }
                })
                return false;       
            }
        });
    }
	
	var hapus = function() {
		var $link = $('.ts_remove_row');
        $link.click(function(event) {
            event.preventDefault();
            var $idLink = '#'+$(this).attr('id');
            UIkit.modal.confirm('Apakah Anda Yakin Akan Hapus Data Master Tagihan Ini?', 
            function(){ 
                $.ajax(
                {
                    url:$($idLink).attr('href'),
                    success:function(data) 
                    {                             
                       $('#response').html(data);   
                       FormValidation.init();   
                    }
                }); 
            });   
        });
    }
	
	
    return {
        //main function to initiate the module
        init: function () {
            if($formValidate.length>0) form_mastertagihan();
			if($formValidateAction.length>0) form_mastertagihan_action();
			hapus();
        }
    };
}();
