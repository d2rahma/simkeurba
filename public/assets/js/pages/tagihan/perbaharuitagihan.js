$(function() {
    // validation
    FormValidation.init();
});

var FormValidation = function () {
    var $formValidate = $('#form_perbaharuitagihan');
	var $formResponse = $('#form_response');

	var $btnTampil = $('#btn-tampil');
	
    var form_perbaharuitagihan = function() {
         $formValidate
            .parsley()
            .on('form:validated',function() {
                altair_md.update_input($formValidate.find('.md-input-danger'));
            })
            .on('field:validated',function(parsleyField) {
                if($(parsleyField.$element).hasClass('md-input')) {
                    altair_md.update_input( $(parsleyField.$element) );
                }
            });

        window.Parsley.on('field:validate', function() {
            var $server_side_error = $(this.$element).closest('.md-input-wrapper').siblings('.error_server_side');
            if($server_side_error) {
                $server_side_error.hide();
            }
        });

        $formValidate.submit(function(e) { 
			var className = $btnTampil.attr('class');
			$btnTampil.html('Sedang Diproses <i class="uk-icon-spinner uk-icon-small uk-icon-spin" id="loading-spinner"></i>');
			$btnTampil.prop('class', 'md-btn disabled');
			
            e.preventDefault();
            if ( $(this).parsley().isValid() ) {
                $.ajax({
                    type: $formValidate.attr('method'),
                    url: $formValidate.attr('action'),
                    data: $formValidate.serialize(),
                    success: function(data) {                        
                        $('#response').html(data);  
						form_response();
						$btnTampil.prop('class', className);						
						$btnTampil.html('Tampil');					
                    }
                })
                return false;       
            }
        });
    }    	
	
	
	 var form_response = function() {
		
		var $formResponse = $('#form_response');
 
		$formResponse
            .parsley()
            .on('form:validated',function() {
                altair_md.update_input($formResponse.find('.md-input-danger'));
            })
            .on('field:validated',function(parsleyField) {
                if($(parsleyField.$element).hasClass('md-input')) {
                    altair_md.update_input( $(parsleyField.$element) );
                }
            });

        window.Parsley.on('field:validate', function() {
            var $server_side_error = $(this.$element).closest('.md-input-wrapper').siblings('.error_server_side');
            if($server_side_error) {
                $server_side_error.hide();
            }
        });

        $formResponse.submit(function(e) {
			
            e.preventDefault();
            if ( $formResponse.parsley().isValid() ) {				
				UIkit.modal.confirm('Apakah Anda Yakin Akan Memperbaharui Tagihan?', 
				function(){ 
				 	$('#response').html('<center><img src="/public/assets/img/spinners/spinner_large.gif" alt="" width="64" height="64"><br/><br/>Sedang Diproses.....</center>');
					$.ajax({
						type: $formResponse.attr('method'),
						url: $formResponse.attr('action'),
						data: $formResponse.serialize(),
						success: function(data) {
						   $('#response').html(data);                
						}
					})
				});                  
                return false;       			
            }
        });
    }
	
	
    return {
        //main function to initiate the module
        init: function () {
            if($formValidate.length>0) form_perbaharuitagihan();			
        }
    };
}();
